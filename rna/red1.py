from numpy import mod
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


dataset = pd.read_csv('bank_customers.csv')
print(dataset)
print()
print(dataset.info())

X = dataset.iloc[:, :-1]
y = dataset.iloc[:, -1]

print()
print(y.head())

states = pd.get_dummies(X['Geography'])
genders = pd.get_dummies(X['Gender'])

X = X.drop(['RowNumber', 'CustomerId', 'Surname', 'Geography', 'Gender'], axis=1)
X = pd.concat([X, states, genders], axis=1)
print(X)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

print(X_train.shape)

from keras.models import Sequential
from keras.layers import Dense

model = Sequential()
model.add(Dense(activation='relu', input_dim=13, units=20))
model.add(Dense(activation='relu', units=20))
model.add(Dense(activation='relu', units=20))
model.add(Dense(activation='sigmoid', units=1))

model.summary()

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

model.fit(X_train, y_train, batch_size=10, epochs=50)

y_pred = model.predict(X_test)

y_pred = (y_pred>0.5)

from sklearn.metrics import confusion_matrix, accuracy_score
cm = confusion_matrix(y_test, y_pred)
acs = accuracy_score(y_test, y_pred)

print(cm)
print(acs)
