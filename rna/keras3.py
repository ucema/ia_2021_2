import numpy as np
np.random.seed(123)  # for reproducibility
 
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist

import matplotlib.pyplot as plt

(X_train, y_train), (X_test, y_test) = mnist.load_data()
X_train_resharped = X_train.reshape(X_train.shape[0],  28, 28, 1)


model = load_model('red_keras2')

print(model.summary())

imgplot = plt.imshow(X_train[4])
plt.show()

x = model.predict(x=[X_train_resharped[0:5]])
print(x)
