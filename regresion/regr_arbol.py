import numpy as np
import matplotlib.pyplot as plt

from sklearn.tree import DecisionTreeRegressor

np.random.seed(0)

def f(x):
    y = 0.1 * np.square(x) + 0.2 * np.random.randn(x.size)
    return y

x = np.arange(-5, 5, 0.1)
y = f(x)

tree = DecisionTreeRegressor()
tree.fit(x.reshape(-1, 1), y)

mediciones = [-0.6, 1.2, 2.4]
mediciones_format = np.array(mediciones).reshape(-1, 1)
pred = tree.predict(mediciones_format)
print(pred)

plt.scatter(x.reshape(-1, 1), y, color='red')
plt.scatter(mediciones, pred, color='blue', linewidth=3)
plt.plot(mediciones, pred, color='green', linewidth=2)
plt.xticks(())
plt.yticks(())
plt.show()