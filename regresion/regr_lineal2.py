from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

diabetes = datasets.load_diabetes()

X_train, X_test, y_train, y_test = train_test_split(
    diabetes.data, 
    diabetes.target, 
    test_size=0.2, 
    random_state=0
)

regresion = linear_model.LinearRegression()
regresion.fit(X_train, y_train)

print(regresion.coef_)
print(regresion.intercept_)

y_pred = regresion.predict(X_test)

error_cuad = mean_squared_error(y_test, y_pred)
print('Error cuad medio: ', error_cuad)

plt.plot(y_test, y_pred, '.')

x = np.linspace(0, 330, 100)
y = x
plt.plot(x, y)
plt.show()



