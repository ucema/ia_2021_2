from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error
import numpy as np    # ndarray
import matplotlib.pyplot as plt


diabetes = datasets.load_diabetes()

X = diabetes.data
y = diabetes.target

X_train = X[:-40]         # Tomar todos menos los ultimos 40 datos
X_test = X[len(X)-40:]  # Toma los ultimos 40 datos

y_train = y[:-40]
y_test = y[len(X)-40:]

regresion = linear_model.LinearRegression()
regresion.fit(X_train, y_train)

# y = mx+b
print(regresion.coef_)

y_pred = regresion.predict(X_test)
print(y_test[:5])
print(y_pred[:5])

error_cuad = mean_squared_error(y_test, y_pred)
print('Error cuad medio: ', error_cuad)

plt.scatter(X_train, y_train, color='red')
plt.scatter(X_test, y_test, color='blue')
plt.plot(X_test, y_pred, color='black')
plt.show()

