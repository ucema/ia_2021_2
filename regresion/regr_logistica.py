from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


digits = load_digits()    # 8 x 8 pixeles

print(digits.data.shape)

plt.figure(figsize=(20, 4))
for index, (image, label) in enumerate(zip(digits.data[10:15], digits.target[10:15])):
    plt.subplot(1, 5, index + 1)
    plt.imshow(np.reshape(image, (8, 8)), cmap=plt.cm.gray)
    plt.title('Digito %i\n' % label, fontsize=20)

# plt.show()


X_train, X_test, y_train, y_test = train_test_split(digits.data, digits.target, test_size=0.20)

lg = LogisticRegression(max_iter=5000)
lg.fit(X_train, y_train)

y_pred = lg.predict(X_test)

matriz_conf = confusion_matrix(y_test, y_pred)
print(matriz_conf)

plt.figure(figsize=(9, 9))
sns.heatmap(matriz_conf, annot=True, fmt='.3f', linewidth=.5, square=True, cmap='Blues')
plt.ylabel('Real')
plt.xlabel('Prediccion')
plt.show()
