from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_iris
from mpl_toolkits.mplot3d import Axes3D

iris = load_iris()
X = iris.data
y = iris.target

k1 = KMeans(n_clusters=3).fit(X)

print(k1.labels_)

fig = plt.figure(0, figsize=(4, 3))
ax = Axes3D(fig, rect=[0, 0, 1, 1], elev=48, azim=130)
labels = k1.labels_
ax.scatter(X[:,3], X[:,0], X[:,2], c=labels.astype(np.float))
ax.w_xaxis.set_ticklabels([])

ax.set_xlabel('Petal width')

plt.show()
