import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.datasets import make_blobs
from itertools import cycle

centers = [[1,1], [-1,-1], [1,-1], [-1,1], [0, 2]]
X, y = make_blobs(n_samples=1000, centers=centers, cluster_std=0.6)


bandwidth = estimate_bandwidth(X, quantile=0.2)
ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
ms.fit(X)

labels = ms.labels_
labels_unique = np.unique(labels)
cluster_centers = ms.cluster_centers_

print(labels_unique)
print(cluster_centers)

# m = labels==1
# print(m)

plt.figure(1)
plt.clf()

colors = cycle('bgrcmyk')
for k, color in zip(range(len(labels_unique)), colors):
     m = labels==k
     cluster_center = cluster_centers[k]
     plt.plot(X[m,0], X[m, 1], color + '.')
     plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=color, markersize=14)

plt.show()
