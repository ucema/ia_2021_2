'''
k grupos
'''
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt

X = np.array([[1, 2], [1, 4], [1, 0], [4,2], [4,4], [4,0]])

km = KMeans(n_clusters=2)
km_entrenado = km.fit(X)

print(km.labels_)
print(km.predict([[1.5, 1]]))
print(km.cluster_centers_)


x = X[:, np.newaxis, 0]
y = X[:, np.newaxis, 1]

fig = plt.figure(0, figsize=(4, 3))
plt.scatter(x, y)

x_c = km.cluster_centers_[:, np.newaxis, 0]
y_c = km.cluster_centers_[:, np.newaxis, 1]
plt.scatter(x_c, y_c, edgecolor='r')
plt.show()

