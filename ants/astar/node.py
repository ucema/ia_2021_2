class Node():
  def __init__(self, id, g, h, parent, x, y):
    self.id = id
    self.g = g
    self.h = h
    self.x = x
    self.y = y
    self.parent = parent

  @property
  def f(self):
    return self.g + self.h

  def __str__(self):
    return '[{}: {} {} {} - {}, {}]'.format(self.id, self.f, self.g, self.h, self.x, self.y)

  # def __eq__(self, value):
  #   return self.x == value.x and self.y == value.y
