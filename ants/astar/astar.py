from node import Node

class AStar():
  def __init__(self, map):    # en map viene una matriz: 1 pared, 0 libre
    self.nodes = []
    self.open_nodes = []
    self.closed_nodes = []
    self.map = map
    id_node = 0
    for x, linea in enumerate(map.split('\n')):
      for y, celda in enumerate(linea):
        if celda == '0':
          node = Node(id_node, 0, 0, None, x, y)
          id_node += 1
          self.nodes.append(node)

  def find_path(self, x0, y0, x1, y1):
    self.open_nodes = []
    self.closed_nodes = []

    self.origin = list(filter(lambda node: node.x == x0 and node.y == y0, self.nodes))[0]
    self.destination = list(filter(lambda node: node.x == x1 and node.y == y1, self.nodes))[0]
    self.open_nodes.append(self.origin)

    current_node = self.origin
    while current_node != self.destination:
      self.process_node(current_node)
      current_node = self.get_min_f_node()

    return self.retrieve_path()

  def process_node(self, node):
    self.open_nodes.remove(node)
    self.closed_nodes.append(node)

    adj_nodes = self.get_adjacent_nodes(node)
    
    for adj in adj_nodes:

      if adj in self.closed_nodes:
        continue

      h = abs(self.destination.x - adj.x) + abs(self.destination.y - adj.y)
      g = node.g
      if node.x == adj.x or node.y == adj.y:
        g += 10
      else:
        g += 14

      if not adj in self.open_nodes:
        adj.g = g
        adj.h = h
        adj.parent = node
        self.open_nodes.append(adj)
      else:
        if (h + g < adj.f):
          adj.g = g
          adj.h = h
          adj.parent = node

  def get_min_f_node(self):
    node = self.open_nodes[0]
    for n in self.open_nodes:
      if n.f < node.f:
        node = n

    return node

  def retrieve_path(self):
    path = []
    node = self.destination
    while node != None:
      path.append(node)
      node = node.parent
    
    return path[::-1]

  def get_adjacent_nodes(self, node_center):
    adjs = []
    n = list(filter(lambda node: node.x == node_center.x + 1 and node.y == node_center.y, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x + 1 and node.y == node_center.y + 1, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x + 1 and node.y == node_center.y - 1, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x and node.y == node_center.y + 1, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x and node.y == node_center.y - 1, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x - 1 and node.y == node_center.y, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x - 1 and node.y == node_center.y - 1, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    n = list(filter(lambda node: node.x == node_center.x - 1 and node.y == node_center.y + 1, self.nodes))
    if len(n) > 0:
      adjs.append(n[0])

    return adjs

map = '''0100010000
0100000000
0111000011
0001000010
0000000000'''

astar = AStar(map)
p = astar.find_path(2,0,3,4)

for n in p:
  print(n)

'''
  0123456789

0 0100010000
1 0100000000
2 0111000011
3 0001000010
4 0000000000
'''