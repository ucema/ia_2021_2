from random import randrange
from food import Food

class Cell():
    def __init__(self, ant=None, food=None, pheromone=None):
        self.ant = ant
        self.food = food
        self.pheromone = pheromone
        
    def __str__(self):
        aux = ''
        if self.ant:              # False: 0, None
            aux += 'A'
        else:
            aux += '.'

        if self.food:              # False: 0, None
            aux += 'F'
        else:
            aux += '.'

        if self.pheromone:              # False: 0, None
            aux += 'P'
        else:
            aux += '.'
          
        return aux

class Anthill():

    def __init__(self, height, width, n_food):
        self.map = [ [ None for i in range(width) ] for j in range(height) ]

        for x in range(width):
            for y in range(height):
              self.map[y][x] = Cell()

        for i in range(n_food):       # Secuencia entre 0 y food-1
                                      # for (int i = 0; i < food; i++)
            x = randrange(width)
            y = randrange(height)
            self.map[y][x].food = Food(10)

    def __str__(self):
        aux = ''
        for line in self.map:
            for cell in line:
                aux += str(cell) + ' '
            aux += '\n'

        return aux

m = Anthill(5, 6, 4)
print(m)