from sklearn import tree
from sklearn.datasets import load_iris

# https://www.kaggle.com/datasets
# https://datasetsearch.research.google.com/

# Features: Largo de sépalo 	
#           Ancho de sépalo
#  	        Largo de pétalo
#        	  Ancho de pétalo 	

# 0- I. setosa
# 1- I. versicolor
# 2- I. virginica

datos_iris = load_iris()

print(datos_iris['data'][:5])
print(datos_iris['target'][:5])

print(datos_iris['target_names'])

data = datos_iris['data']
target = datos_iris['target']


clasificador = tree.DecisionTreeClassifier()
clasificador.fit(data, target)

a = [5.09, 3.47, 1.01, 0.207]
pred = clasificador.predict([a])
print(pred)
print(datos_iris['target_names'][pred[0]])


import graphviz

puntos = tree.export_graphviz(clasificador)
graph = graphviz.Source(puntos, format='png')
graph.render('arbol2')

