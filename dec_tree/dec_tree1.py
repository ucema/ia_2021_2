from sklearn import tree
import graphviz

# Features: altura, peso, numero calzado
data = [
    [181, 80, 44], [177, 70, 43], [160, 60, 38], [154, 54, 37], 
    [190, 90, 47], [175, 64, 39], [166, 65, 40],
    [177, 70, 40], [159, 55, 37], [171, 75, 42], [181, 85, 43]
]

target = [
    'hombre', 'hombre', 'mujer', 'mujer', 
    'hombre', 'hombre', 'mujer', 
    'mujer', 'mujer', 'hombre', 'hombre'
]

clasificador = tree.DecisionTreeClassifier()

# Entrenar el clasificador
clasificador.fit(data, target)

a = [178, 71, 43]
b = [158, 50, 37]
prediccion = clasificador.predict([a, b])
print(prediccion)

puntos = tree.export_graphviz(clasificador)
graph = graphviz.Source(puntos, format='png')
graph.render('arbol1')



'''
Data scients

Negro --->   0
Blanco --->  1
Azul --->    2
'''